package com.example.main;

public class Contact {
	private String description; //es. "e-mail", "num telefono" ecc.
	private String value;		//es. "xxx.yyy@gmail.com", "+393330011223"
	
	public Contact(String description, String value) {
		this.description = description;
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Contact [description=\"" + description + "\", value=\"" + value + "\"]";
	}
	
	
}
