package com.example.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

import com.example.dao.MongoDao;
import com.example.utility.Converter;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.sun.corba.se.impl.util.Utility;

public class Main {

	public static void main(String[] args) {
		
		MongoDao dao = MongoDao.getInstance();
		
		dao.dropCollection("Person"); // facciamo un bel clean :)
		
		populatePersons(dao);
		
		// prendiamo tutte le Person del DB e le stampiamo in console
		DBCursor cursorAllPersons = dao.getAllRows("Person");
		ArrayList<Person> allPersons = getPersonArrayFromCursor(cursorAllPersons);
		printPersons(allPersons);
		
		BufferedReader console = new BufferedReader(new java.io.InputStreamReader(System.in));
		
		String id="";
		try {
			System.out.print("---\nInserire l'id della Person a cui aggiornare l'età: ");
			id = console.readLine(); // id della persona da leggere nel db per modificargli l'età
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("\n*******************GET Person by id\n");
		DBCursor cursor = dao.findByClause("Person", new BasicDBObject("id", id));
		Person p = getPersonArrayFromCursor(cursor).get(0); 
		System.out.println(p);
		
		System.out.println("\n*******************UPDATE Person by id:\n");
		
		p.setAge(30); // modifica per testare update
		dao.updatePerson(id, p);
		
		cursor = dao.findByClause("Person", new BasicDBObject("id", id));
		p = getPersonArrayFromCursor(cursor).get(0); 
		System.out.println(p);
		
		/////////////////////////////////////////////////////////
		
		System.out.println("\n*******************getPersonsWithTheseAges (18 and 28) RESULTS:\n");
		
		ArrayList<Integer> ages = new ArrayList<>();
		ages.add(18);
		ages.add(28);
		cursor = dao.getPersonsWithTheseAges(ages);
		ArrayList<Person> persons = getPersonArrayFromCursor(cursor);
		printPersons(persons);

		
	}
	
	public static ArrayList<Person> getPersonArrayFromCursor(DBCursor cursor){
		ArrayList<Person> persons = new ArrayList<>();
		while (cursor.hasNext()) {
			Person p = null;
			BasicDBObject personObj = (BasicDBObject) cursor.next();
			p = Converter.convertBasicDBObject2Person(personObj);
			persons.add(p);
		}
		return persons;
	}
	
	
	public static void populatePersons(MongoDao dao){
		////////////////////PRIMA PERSONA/////////////////////////
		ArrayList<Contact> contacts = new ArrayList<>();
		contacts.add(new Contact("email", "xxx@yyy.it"));
		contacts.add(new Contact("telefono", "333445566"));
		
		ArrayList<String> hobbies = new ArrayList<>();
		hobbies.add("Fotografia");
		hobbies.add("Cinema");
		hobbies.add("Viaggi");
		
		dao.createPerson(new Person("Marco", "Bianchi", 28, hobbies, contacts));
		//////////////////////////////////////////////////////////

		
		////////////////////SECONDA PERSONA/////////////////////////
		ArrayList<Contact> contacts2 = new ArrayList<>();
		contacts2.add(new Contact("telefono", "333882211"));
		
		ArrayList<String> hobbies2 = new ArrayList<>();
		hobbies2.add("Corsa");
		
		dao.createPerson(new Person("Riccardo", "Verdi", 18, hobbies2, contacts2));
		//////////////////////////////////////////////////////////

		
		////////////////////TERZA PERSONA/////////////////////////
		ArrayList<Contact> contacts3 = new ArrayList<>();
		contacts3.add(new Contact("email", "zzz@www.it"));
		
		ArrayList<String> hobbies3 = new ArrayList<>();
		hobbies3.add("Corsa");
		hobbies3.add("Lettura");
		
		dao.createPerson(new Person("Giovanni", "Rossi", 35, hobbies3, contacts3));
		//////////////////////////////////////////////////////////
	}
	
	
	public static void printPersons (ArrayList<Person> persons){
		for(int i=0; i<persons.size(); i++){
			System.out.println(persons.get(i));
		}
	}

}
