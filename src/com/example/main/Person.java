package com.example.main;

import java.util.ArrayList;

public class Person {
	
	private String id;
	private String name;
	private String surname;
	private Integer age;
	private ArrayList<String> hobbies;
	private ArrayList<Contact> contacts;
			
	public Person(){
		id = java.util.UUID.randomUUID().toString();
		name = "";
		surname = "";
		age = 0;
		hobbies = null;
		contacts = null;
	}
	
	public Person(String name, String surname, Integer age, ArrayList<String> hobbies, ArrayList<Contact> contacts) {
		id = java.util.UUID.randomUUID().toString();
		this.name = name;
		this.surname = surname;
		this.age = age;
		this.hobbies = hobbies;
		this.contacts = contacts;
	}
	

	public Person(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
	
	public ArrayList<String> getHobbies() {
		return hobbies;
	}

	public void setHobbies(ArrayList<String> hobbies) {
		this.hobbies = hobbies;
	}

	public void addHobby(String hobby){
		hobbies.add(hobby);
	}
	
	public ArrayList<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(ArrayList<Contact> contacts) {
		this.contacts = contacts;
	}
	
	public void addContact(Contact contact){
		contacts.add(contact);
	}

	@Override
	public String toString() {
		String s = "Person [\n\tid=\""+id+"\",\n"
				+ "\tname=\"" + name + "\",\n"
				+ "\tsurname=\"" + surname + "\",\n"
				+ "\tage=" + age + ",\n"
				+ "\thobbies=[";
		for(int i=0; i<hobbies.size(); i++){
			s+="\""+hobbies.get(i)+"\"";
			if(i<hobbies.size()-1)s+=", ";
		}
		s+="], \n\tcontacts=[\n";
		for(int i=0; i<contacts.size(); i++){
			s+="\t\t"+contacts.get(i).toString()+"\n";
		}
		s+="\t]\n]";
		return s;
	}
	

}
