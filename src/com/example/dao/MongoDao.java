package com.example.dao;

import java.util.ArrayList;
import java.util.Set;

import com.example.main.Person;
import com.example.utility.Converter;
import com.example.utility.Parameters;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class MongoDao {

	private MongoClient connection = null;
	private DB db = null;
	
	private static MongoDao mongoDao = null;

	private MongoDao(){
		//Connection is created as below
		connection= new MongoClient(Parameters.addressDB, Parameters.portDB);
		db = connection.getDB(Parameters.nameDB);
	}
	
	// Singleton per ottenere la istanza di connessione al DB
	public static MongoDao getInstance(){
		if(mongoDao==null){
			mongoDao = new MongoDao();
		}
		return mongoDao;
	}

	// Come crare una collezione (tabella) nel caso non esista
	public void createTable(String collName){
		Set collNames = db.getCollectionNames(); //restituisce i nomi delle tabelle
		if(!collNames.contains(collName)){
			DBObject dbobject = new BasicDBObject();
			db.createCollection(collName, dbobject);
		}
	}

	/*
	Inserimento di un documento nel DB
	Ogni record è un DBObject che non è altro essere una mappa che contiene coppie chiave valore. Ogni chiave rappresenta il nome dell'attributo nella collezione (nome colonna)
	 */
	public void saveToDB(String collName, BasicDBObject dbObject){
		DBCollection dbCollection = db.getCollection(collName);
		dbCollection.insert(dbObject);
	}

	// Restituisce la lista delle collezioni nel DB
	public Set getcollNames()throws Exception{
		return db.getCollectionNames();
	}

	// Restituisce tutti i documenti (righe) di una collezione (tabella)
	public DBCursor getAllRows(String collName){
		DBCollection dbCollection = db.getCollection(collName);
		DBCursor cur = dbCollection.find();
		return cur;
	}

	// Restituisce il numero di documenti di una collezione
	public int getRowCount(String collName){
		DBCollection dbCollection = db.getCollection(collName);
		DBCursor cur = dbCollection.find();
		return cur.count();
	}
	
	// Verificare se una collezione è presente nel DB
	public boolean exists(String collName, BasicDBObject query){
		DBCollection dbCollection = db.getCollection(collName);
		DBCursor cur = dbCollection.find(query);
		return cur.count()==0? false : true;
	}

	// Cerca tramite clausola where (sotto forma di BasicDBOBject - es. new BasicDBObject("id", id))
	public DBCursor findByClause(String collName, BasicDBObject whereClause){
		DBCursor result = null;
		DBCollection dbCollection = db.getCollection(collName);
		result = dbCollection.find(whereClause);
		return result;
	}

	// Crea l'indice in funzione di un certo attributo della collezione (utile quando abbiamo una grande mole di dati per velocizzare la ricereca
	public void createIndex(String collName, String columnName){
		DBCollection dbCollection = db.getCollection(collName);
		DBObject indexData = new BasicDBObject(columnName,1);
		dbCollection.createIndex(indexData);
	}

	// Cancella la collezione
	public void dropCollection(String collectionName){
		db.getCollection(collectionName).drop();
	}
	
	// Restituisce l'oggetto collection
	public DBCollection getCollection(String collName){
		return db.getCollection(collName);
	}

	// Creazione Person nel DB
	public void createPerson(Person person){
		BasicDBObject obj = Converter.convertPerson2BasicDBObject(person);
		try {
			saveToDB("Person", obj);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("*** Problem in "+e.getStackTrace()[0].getMethodName());
		}		
	}
	
	// Aggiornamento Person nel DB
	public void updatePerson(String string, Person person){
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.append("id", string);
		BasicDBObject obj = Converter.convertPerson2BasicDBObject(person);
		
		BasicDBObject set = new BasicDBObject("$set", obj); 
		try {
			getCollection("Person").update(searchQuery, set);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("*** Problem in "+e.getStackTrace()[0].getMethodName());
		}
	}
	
	// Rimozione Person dal DB
	public void removePerson(String id){
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.append("id", id);
		try {
			getCollection("Person").remove(searchQuery);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("*** Problem in "+e.getStackTrace()[0].getMethodName());
		}
	}
	
	// Restitusice l'insieme delle Person con dei nomi specifici
	public DBCursor getPersonsWithTheseAges(ArrayList<Integer> ages){
		BasicDBList clauses = new BasicDBList();
		for (int i = 0; i < ages.size(); i++) {
			clauses.add(new BasicDBObject("age", ages.get(i)));
		}
		DBObject searchQuery = new BasicDBObject("$or", clauses);
		try {
			return getCollection("Person").find(searchQuery);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("*** Problem in "+e.getStackTrace()[0].getMethodName());
			return null;
		}
	}

}
