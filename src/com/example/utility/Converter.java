package com.example.utility;

import java.util.ArrayList;

import com.example.main.Contact;
import com.example.main.Person;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

public class Converter {

	public static BasicDBObject convertPerson2BasicDBObject(Person person){
		BasicDBObject obj = new BasicDBObject();
		obj.put("id", person.getId());
		obj.put("name", person.getName());
		obj.put("surname", person.getSurname());
		obj.put("age", person.getAge());
		obj.put("hobbies", person.getHobbies());
		obj.put("contacts", convertContacts2BasicDBList(person.getContacts()));
		return obj;
	}
	
	private static BasicDBList convertContacts2BasicDBList(ArrayList<Contact> contacts) {
		BasicDBList list = new BasicDBList();
		for(int i=0; i<contacts.size(); i++){
			BasicDBObject obj = new BasicDBObject();
			obj.put("description", contacts.get(i).getDescription());
			obj.put("value", contacts.get(i).getValue());
			list.add(obj);
		}
		BasicDBObject contactsObj = new BasicDBObject();
		contactsObj.append("contacts", list);
		return list;
	}

	public static Person convertBasicDBObject2Person(BasicDBObject personObj){
		Person person = new Person(personObj.getString("id"));
		person.setName(personObj.getString("name"));
		person.setSurname(personObj.getString("surname"));
		person.setAge(personObj.getInt("age"));
		person.setHobbies((ArrayList<String>)personObj.get("hobbies"));
		BasicDBList contactsList = (BasicDBList)personObj.get("contacts");
		person.setContacts(convertBasicDBList2Contacts(contactsList));
		return person;
	}
	
	private static ArrayList<Contact> convertBasicDBList2Contacts(BasicDBList contactsList) {
		ArrayList<Contact> contacts = new ArrayList<>();
		for(Object el : contactsList){
			String description = ((BasicDBObject)el).getString("description");
			String value = ((BasicDBObject)el).getString("value");
			contacts.add(new Contact(description, value));
		}
		return contacts;
	}
}
